package net.amunak.minecraft.playerhighlighter;

import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;

public class KeyHandler {
	private static Minecraft mc = Minecraft.getMinecraft();

	private static final ArrayList<KeyBinding> keys = new ArrayList<>();

	KeyHandler() {
		keys.add(new KeyBinding("Toggle lines", Keyboard.KEY_O, PlayerHighlighter.NAME));

		for (KeyBinding key : keys) {
			ClientRegistry.registerKeyBinding(key);
		}
	}

	@SubscribeEvent
	public void onKeyInput(InputEvent.KeyInputEvent event) {
		// checking inGameHasFocus prevents your keys from firing when the player is typing a chat message
		if (!mc.inGameHasFocus) {
			return;
		}

		if (keys.get(0).isKeyDown()) {
			PlayerHighlighter.logger.info("Toggle key pressed");
			Renderer.drawLines = !Renderer.drawLines;
		}
	}
}
