package net.amunak.minecraft.playerhighlighter;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.IModGuiFactory;
import net.minecraftforge.fml.client.config.DummyConfigElement;
import net.minecraftforge.fml.client.config.GuiConfig;
import net.minecraftforge.fml.client.config.GuiConfigEntries;
import net.minecraftforge.fml.client.config.IConfigElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ModConfigGuiFactory implements IModGuiFactory {
	@Override
	public void initialize(Minecraft minecraftInstance) {

	}

	@Override
	public boolean hasConfigGui() {
		return true;
	}

	@Override
	public GuiScreen createConfigGui(GuiScreen parentScreen) {
		return new ConfigScreen(parentScreen);
	}

	@Override
	public Set<RuntimeOptionCategoryElement> runtimeGuiCategories() {
		return null;
	}

	public static class ConfigScreen extends GuiConfig {
		GuiScreen parent;

		ConfigScreen(GuiScreen parent) {
			super(parent, getConfigElements(), PlayerHighlighter.MODID, false, false, PlayerHighlighter.NAME);
			this.parent = parent;
		}

		private static List<IConfigElement> getConfigElements()
		{
			List<IConfigElement> list = new ArrayList<>();
			// Add the two buttons that will go to each config category edit screen
			list.add(new DummyConfigElement.DummyCategoryElement(ModConfig.CATEGORY_LINES, ModConfig.CATEGORY_LINES, LinesCategory.class));

			// @todo this is fucking retarded - figure out a way to do this automatically and without a new class for every fucking category
			list.add(new DummyConfigElement.DummyCategoryElement(ModConfig.CATEGORY_ENTITIES_PLAYERS, ModConfig.CATEGORY_ENTITIES_PLAYERS, EntitiesPlayersCategory.class));
			list.add(new DummyConfigElement.DummyCategoryElement(ModConfig.CATEGORY_ENTITIES_CREATURES, ModConfig.CATEGORY_ENTITIES_CREATURES, EntitiesCreaturesCategory.class));
			list.add(new DummyConfigElement.DummyCategoryElement(ModConfig.CATEGORY_ENTITIES_MONSTERS, ModConfig.CATEGORY_ENTITIES_MONSTERS, EntitiesMonstersCategory.class));
			list.add(new DummyConfigElement.DummyCategoryElement(ModConfig.CATEGORY_ENTITIES_OTHER, ModConfig.CATEGORY_ENTITIES_OTHER, EntitiesOtherCategory.class));

			return list;
		}

		public static class LinesCategory extends SimpleCategory {
			public LinesCategory(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement prop) {
				super(owningScreen, owningEntryList, prop);
			}

			@Override
			String getCategoryName() {
				return ModConfig.CATEGORY_LINES;
			}
		}

		public static class EntitiesPlayersCategory extends SimpleCategory {
			public EntitiesPlayersCategory(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement prop) {
				super(owningScreen, owningEntryList, prop);
			}

			@Override
			String getCategoryName() {
				return ModConfig.CATEGORY_ENTITIES_PLAYERS;
			}
		}

		public static class EntitiesCreaturesCategory extends SimpleCategory {
			public EntitiesCreaturesCategory(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement prop) {
				super(owningScreen, owningEntryList, prop);
			}

			@Override
			String getCategoryName() {
				return ModConfig.CATEGORY_ENTITIES_CREATURES;
			}
		}

		public static class EntitiesMonstersCategory extends SimpleCategory {
			public EntitiesMonstersCategory(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement prop) {
				super(owningScreen, owningEntryList, prop);
			}

			@Override
			String getCategoryName() {
				return ModConfig.CATEGORY_ENTITIES_MONSTERS
						;
			}
		}

		public static class EntitiesOtherCategory extends SimpleCategory {
			public EntitiesOtherCategory(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement prop) {
				super(owningScreen, owningEntryList, prop);
			}

			@Override
			String getCategoryName() {
				return ModConfig.CATEGORY_ENTITIES_OTHER;
			}
		}

		public static abstract class SimpleCategory extends GuiConfigEntries.CategoryEntry
		{
			public SimpleCategory(GuiConfig owningScreen, GuiConfigEntries owningEntryList, IConfigElement configElement) {
				super(owningScreen, owningEntryList, configElement);
			}

			abstract String getCategoryName();

			@Override
			protected GuiScreen buildChildScreen()
			{
				Configuration configuration = ModConfig.instance.config;
				ConfigElement categoryLines = new ConfigElement(configuration.getCategory(getCategoryName()));
				List<IConfigElement> propertiesOnThisScreen = categoryLines.getChildElements();
				String windowTitle = String.format("%s - %s", PlayerHighlighter.NAME, I18n.format(getCategoryName()));

				return new GuiConfig(this.owningScreen, propertiesOnThisScreen,
						this.owningScreen.modID,
						getCategoryName(),
						this.configElement.requiresWorldRestart() || this.owningScreen.allRequireWorldRestart,
						this.configElement.requiresMcRestart() || this.owningScreen.allRequireMcRestart,
						windowTitle);
			}
		}
	}
}
