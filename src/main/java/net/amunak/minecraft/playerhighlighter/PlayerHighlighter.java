package net.amunak.minecraft.playerhighlighter;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;

@Mod(modid = PlayerHighlighter.MODID, name = PlayerHighlighter.NAME, version = PlayerHighlighter.VERSION, guiFactory = "net.amunak.minecraft.playerhighlighter.ModConfigGuiFactory")
public class PlayerHighlighter {
	public static final String MODID = "playerhighlighter";
	public static final String NAME = "Player Highlighter";
	public static final String VERSION = "1.1.7";

	static Logger logger;

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		logger = event.getModLog();

		ModConfig.instance.loadDefaultConfig(event);
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(Renderer.instance);
		MinecraftForge.EVENT_BUS.register(new KeyHandler());
	}
}
