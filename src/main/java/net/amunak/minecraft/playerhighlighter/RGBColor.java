package net.amunak.minecraft.playerhighlighter;

class RGBColor {
	float red = 0;
	float green = 0;
	float blue = 0;

	RGBColor(){}

	RGBColor(float red, float green, float blue)
	{
		this.red = red;
		this.green = green;
		this.blue = blue;
	}
}
